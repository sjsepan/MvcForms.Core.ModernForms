﻿using System;
using System.Reflection;
using Ssepan.Application.Core.ModernForms;
using Ssepan.Utility.Core;

namespace MvcForms.Core.ModernForms
{
    partial class MvcView
    {
        // /// <summary>
        // /// Required designer variable.
        // /// </summary>
        // private System.ComponentModel.IContainer components = null;

        // /// <summary>
        // /// Clean up any resources being used.
        // /// </summary>
        // /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        // protected /* override */ void Dispose(bool disposing)
        // {
        //     if (disposing && (components != null))
        //     {
        //         components.Dispose();
        //     }
        //     // base.Dispose(disposing);
        // }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            try//not canonical, but trap errors
            {
                // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MvcView));

                this.menu = new Modern.Forms.Menu();
                this.menuFile = new Modern.Forms.MenuItem();
                this.menuFileNew = new Modern.Forms.MenuItem();
                this.menuFileOpen = new Modern.Forms.MenuItem();
                this.menuFileSave = new Modern.Forms.MenuItem();
                this.menuFileSaveAs = new Modern.Forms.MenuItem();
                this.menuFileSeparator1 = new Modern.Forms.MenuSeparatorItem();
                this.menuFilePrint = new Modern.Forms.MenuItem();
                this.menuFilePrintPreview = new Modern.Forms.MenuItem();
                this.menuFileSeparator2 = new Modern.Forms.MenuSeparatorItem();
                this.menuFileExit = new Modern.Forms.MenuItem();
                this.menuEdit = new Modern.Forms.MenuItem();
                this.menuEditUndo = new Modern.Forms.MenuItem();
                this.menuEditRedo = new Modern.Forms.MenuItem();
                this.menuEditSeparator0 = new Modern.Forms.MenuSeparatorItem();
                this.menuEditSelectAll = new Modern.Forms.MenuItem();
                this.menuEditCut = new Modern.Forms.MenuItem();
                this.menuEditCopy = new Modern.Forms.MenuItem();
                this.menuEditPaste = new Modern.Forms.MenuItem();
				this.menuEditPasteSpecial = new Modern.Forms.MenuItem();
				this.menuEditDelete = new Modern.Forms.MenuItem();
                this.menuEditSeparator1 = new Modern.Forms.MenuSeparatorItem();
                this.menuEditFind = new Modern.Forms.MenuItem();
                this.menuEditReplace = new Modern.Forms.MenuItem();
                this.menuEditSeparator2 = new Modern.Forms.MenuSeparatorItem();
                this.menuEditRefresh = new Modern.Forms.MenuItem();
                this.menuEditSeparator3 = new Modern.Forms.MenuSeparatorItem();
                this.menuEditPreferences = new Modern.Forms.MenuItem();
                this.menuEditProperties = new Modern.Forms.MenuItem();
                this.menuWindow = new Modern.Forms.MenuItem();
                this.menuWindowNewWindow = new Modern.Forms.MenuItem();
                this.menuWindowTile = new Modern.Forms.MenuItem();
                this.menuWindowCascade = new Modern.Forms.MenuItem();
                this.menuWindowArrangeAll = new Modern.Forms.MenuItem();
                this.menuWindowSeparator0 = new Modern.Forms.MenuSeparatorItem();
                this.menuWindowHide = new Modern.Forms.MenuItem();
                this.menuWindowShow = new Modern.Forms.MenuItem();
                this.menuHelpContents = new Modern.Forms.MenuItem();
                this.menuHelpIndex = new Modern.Forms.MenuItem();
                this.menuHelpOnlineHelp = new Modern.Forms.MenuItem();
                this.menuHelpSeparator1 = new Modern.Forms.MenuSeparatorItem();
                this.menuHelpLicenceInformation = new Modern.Forms.MenuItem();
                this.menuHelpCheckForUpdates = new Modern.Forms.MenuItem();
                this.menuHelpSeparator2 = new Modern.Forms.MenuSeparatorItem();
                this.menuHelpAbout = new Modern.Forms.MenuItem();
                this.menuHelp = new Modern.Forms.MenuItem();
                this.toolBar = new Modern.Forms.ToolBar();
                this.buttonFileNew = new Modern.Forms.MenuItem();
                this.buttonFileOpen = new Modern.Forms.MenuItem();
                this.buttonFileSave = new Modern.Forms.MenuItem();
                this.buttonFilePrint = new Modern.Forms.MenuItem();
                this.toolBarSeparator0 = new Modern.Forms.MenuSeparatorItem();
                this.buttonEditUndo = new Modern.Forms.MenuItem();
                this.buttonEditRedo = new Modern.Forms.MenuItem();
                this.buttonEditCut = new Modern.Forms.MenuItem();
                this.buttonEditCopy = new Modern.Forms.MenuItem();
                this.buttonEditPaste = new Modern.Forms.MenuItem();
                this.buttonEditDelete = new Modern.Forms.MenuItem();
                this.buttonEditFind = new Modern.Forms.MenuItem();
                this.buttonEditReplace = new Modern.Forms.MenuItem();
                this.buttonEditRefresh = new Modern.Forms.MenuItem();
                this.buttonEditPreferences = new Modern.Forms.MenuItem();
                this.buttonEditProperties = new Modern.Forms.MenuItem();
                this.toolBarSeparator1 = new Modern.Forms.MenuSeparatorItem();
                this.buttonHelpContents = new Modern.Forms.MenuItem();
                this.statusBar = new Modern.Forms.Panel();
                this.StatusMessage = new Modern.Forms.Label();
                this.ErrorMessage = new Modern.Forms.Label();
                this.ProgressBar = new Modern.Forms.ProgressBar();
                this.ActionIcon = new Modern.Forms.PictureBox();
                this.DirtyIcon = new Modern.Forms.PictureBox();
                this.cmdRun = new Modern.Forms.Button();
                this.cmdFont = new Modern.Forms.Button();
                this.cmdColor = new Modern.Forms.Button();
                this.lblSomeInt = new Modern.Forms.Label();
                this.lblSomeString = new Modern.Forms.Label();
                this.lblSomeBoolean = new Modern.Forms.Label();
                this.txtSomeInt = new Modern.Forms.TextBox();
                this.txtSomeString = new Modern.Forms.TextBox();
                this.chkSomeBoolean = new Modern.Forms.CheckBox();
                this.lblSomeOtherInt = new Modern.Forms.Label();
                this.lblSomeOtherString = new Modern.Forms.Label();
                this.lblSomeOtherBoolean = new Modern.Forms.Label();
                this.txtSomeOtherInt = new Modern.Forms.TextBox();
                this.txtSomeOtherString = new Modern.Forms.TextBox();
                this.chkSomeOtherBoolean = new Modern.Forms.CheckBox();
                this.lblStillAnotherInt = new Modern.Forms.Label();
                this.lblStillAnotherString = new Modern.Forms.Label();
                this.lblStillAnotherBoolean = new Modern.Forms.Label();
                this.txtStillAnotherInt = new Modern.Forms.TextBox();
                this.txtStillAnotherString = new Modern.Forms.TextBox();
                this.chkStillAnotherBoolean = new Modern.Forms.CheckBox();
                // this.toolbar.SuspendLayout();
                this.menu.SuspendLayout();
                this.statusBar.SuspendLayout();
                // this.SuspendLayout();
                //
                // menuFileNew
                //
                this.menuFileNew.Text = "New";
                this.menuFileNew.Image = ImageLoader.Get("New.png");
                this.menuFileNew.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileNew_Click);
                //
                // menuFileOpen
                //
                this.menuFileOpen.Text = "Open";
                this.menuFileOpen.Image = ImageLoader.Get("Open.png");
                this.menuFileOpen.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileOpen_Click);
                //
                // menuFileSave
                //
                this.menuFileSave.Text = "Save";
                this.menuFileSave.Image = ImageLoader.Get("Save.png");
                this.menuFileSave.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileSave_Click);
                //
                // menuFileSaveAs
                //
                this.menuFileSaveAs.Text = "Save As";
                this.menuFileSaveAs.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileSaveAs_Click);
                //
                // menuFileSeparator1
                //
                // this.menuFileSeparator1.Enabled = true;
                //
                // menuFilePrint
                //
                this.menuFilePrint.Text = "Print";
                this.menuFilePrint.Image = ImageLoader.Get("Print.png");
                this.menuFilePrint.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFilePrint_Click);
                //
                // menuFilePrintPreview
                //
                this.menuFilePrintPreview.Text = "Print Preview...";
                this.menuFilePrintPreview.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFilePrintPreview_Click);
                //
                // menuFileSeparator2
                //
                // this.menuFileSeparator2.Enabled = true;
                //
                // menuFileExit
                //
                this.menuFileExit.Text = "Exit";
                this.menuFileExit.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileExit_Click);
                //
                // menuFile
                //
                this.menuFile.Text = "File";
                this.menuFile.Items.Add(this.menuFileNew);
                this.menuFile.Items.Add(this.menuFileOpen);
                this.menuFile.Items.Add(this.menuFileSave);
                this.menuFile.Items.Add(this.menuFileSaveAs);
                this.menuFile.Items.Add(this.menuFileSeparator1);
                this.menuFile.Items.Add(this.menuFilePrint);
                this.menuFile.Items.Add(this.menuFilePrintPreview);
                this.menuFile.Items.Add(this.menuFileSeparator2);
                this.menuFile.Items.Add(this.menuFileExit);
                //
                // menuEditUndo
                //
                this.menuEditUndo.Text = "Undo";
                this.menuEditUndo.Image = ImageLoader.Get("Undo.png");
                this.menuEditUndo.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditUndo_Click);
                //
                // menuEditRedo
                //
                this.menuEditRedo.Text = "Redo";
                this.menuEditRedo.Image = ImageLoader.Get("Redo.png");
                this.menuEditRedo.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditRedo_Click);
                // 
                // menuEditSeparator0
                // 
                // this.menuEditSeparator0.Enabled = true;
                //
                // menuEditSelectAll
                //
                this.menuEditSelectAll.Text = "Select All";
                this.menuEditSelectAll.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditSelectAll_Click);
                //
                // menuEditCut
                //
                this.menuEditCut.Text = "Cut";
                this.menuEditCut.Image = ImageLoader.Get("Cut.png");
                this.menuEditCut.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditCut_Click);
                //
                // menuEditCopy
                //
                this.menuEditCopy.Text = "Copy";
                this.menuEditCopy.Image = ImageLoader.Get("Copy.png");
                this.menuEditCopy.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditCopy_Click);
                //
                // menuEditPaste
                //
                this.menuEditPaste.Text = "Paste";
                this.menuEditPaste.Image = ImageLoader.Get("Paste.png");
                this.menuEditPaste.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditPaste_Click);
                //
                // menuEditPasteSpecial
                //
                this.menuEditPasteSpecial.Text = "Paste Special...";
                this.menuEditPasteSpecial.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditPasteSpecial_Click);
                //
                // menuEditDelete
                //
                this.menuEditDelete.Text = "Delete";
                this.menuEditDelete.Image = ImageLoader.Get("Delete.png");
                this.menuEditDelete.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditDelete_Click);
                // 
                // menuEditSeparator1
                // 
                // this.menuEditSeparator1.Enabled = true;
                //
                // menuEditFind
                //
                this.menuEditFind.Text = "Find";
                this.menuEditFind.Image = ImageLoader.Get("Find.png");
                this.menuEditFind.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditFind_Click);
                //
                // menuEditReplace
                //
                this.menuEditReplace.Text = "Replace";
                this.menuEditReplace.Image = ImageLoader.Get("Replace.png");
                this.menuEditReplace.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditReplace_Click);
                // 
                // menuEditSeparator2
                // 
                // this.menuEditSeparator2.Enabled = true;
                //
                // menuEditRefresh
                //
                this.menuEditRefresh.Text = "Refresh";
                this.menuEditRefresh.Image = ImageLoader.Get("Reload.png");
                this.menuEditRefresh.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditRefresh_Click);
                // 
                // menuEditSeparator3
                // 
                // this.menuEditSeparator3.Enabled = true;
                //
                // menuEditPreferences
                //
                this.menuEditPreferences.Text = "Preferences";
                this.menuEditPreferences.Image = ImageLoader.Get("Preferences.png");
                this.menuEditPreferences.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditPreferences_Click);
                //
                // menuEditProperties
                //
                this.menuEditProperties.Text = "Properties";
                this.menuEditProperties.Image = ImageLoader.Get("Properties.png");
                this.menuEditProperties.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditProperties_Click);
                //
                // menuEdit
                //
                this.menuEdit.Text = "Edit";
                this.menuEdit.Items.Add(this.menuEditUndo);
                this.menuEdit.Items.Add(this.menuEditRedo);
                this.menuEdit.Items.Add(this.menuEditSeparator0);
                this.menuEdit.Items.Add(this.menuEditSelectAll);
                this.menuEdit.Items.Add(this.menuEditCut);
                this.menuEdit.Items.Add(this.menuEditCopy);
                this.menuEdit.Items.Add(this.menuEditPaste);
                this.menuEdit.Items.Add(this.menuEditPasteSpecial);
                this.menuEdit.Items.Add(this.menuEditDelete);
                this.menuEdit.Items.Add(this.menuEditSeparator1);
                this.menuEdit.Items.Add(this.menuEditFind);
                this.menuEdit.Items.Add(this.menuEditReplace);
                this.menuEdit.Items.Add(this.menuEditSeparator2);
                this.menuEdit.Items.Add(this.menuEditRefresh);
                this.menuEdit.Items.Add(this.menuEditSeparator3);
                this.menuEdit.Items.Add(this.menuEditPreferences);
                this.menuEdit.Items.Add(this.menuEditProperties);
                //
                // menuWindowNewWindow
                //
                this.menuWindowNewWindow.Text = "New Window";
                this.menuWindowNewWindow.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowNewWindow_Click);
                //
                // menuWindowTile
                //
                this.menuWindowTile.Text = "Tile";
                this.menuWindowTile.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowTile_Click);
                //
                // menuWindowCascade
                //
                this.menuWindowCascade.Text = "Cascade";
                this.menuWindowCascade.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowCascade_Click);
                //
                // menuWindowArrangeAll
                //
                this.menuWindowArrangeAll.Text = "Arrange All";
                this.menuWindowArrangeAll.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowArrangeAll_Click);
                //
                // menuWindowSeparator0
                //
                // this.menuWindowSeparator0.Enabled = true;
                //
                // menuWindowHide
                //
                this.menuWindowHide.Text = "Hide";
                this.menuWindowHide.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowHide_Click);
                //
                // menuWindowShow
                //
                this.menuWindowShow.Text = "Show";
                this.menuWindowShow.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuWindowShow_Click);
                //
                // menuWindow
                //
                this.menuWindow.Text = "Window";
                this.menuWindow.Items.Add(this.menuWindowNewWindow);
                this.menuWindow.Items.Add(this.menuWindowTile);
                this.menuWindow.Items.Add(this.menuWindowCascade);
                this.menuWindow.Items.Add(this.menuWindowArrangeAll);
                this.menuWindow.Items.Add(this.menuWindowSeparator0);
                this.menuWindow.Items.Add(this.menuWindowHide);
                this.menuWindow.Items.Add(this.menuWindowShow);
                //
                // menuHelpContents
                //
                this.menuHelpContents.Text = "Contents";
                this.menuHelpContents.Image = ImageLoader.Get("Contents.png");
                this.menuHelpContents.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpContents_Click);
                //
                // menuHelpIndex
                //
                this.menuHelpIndex.Text = "Index";
                this.menuHelpIndex.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpIndex_Click);
                //
                // menuHelpOnlineHelp
                //
                this.menuHelpOnlineHelp.Text = "Online Help";
                this.menuHelpOnlineHelp.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpOnlineHelp_Click);
                // 
                // menuHelpSeparator1
                // 
                // this.menuHelpSeparator1.Enabled = true;
                //
                // menuHelpLicenceInformation
                //
                this.menuHelpLicenceInformation.Text = "Licence Information";
                this.menuHelpLicenceInformation.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpLicenceInformation_Click);
                //
                // menuHelpCheckForUpdates
                //
                this.menuHelpCheckForUpdates.Text = "Check For Updates";
                this.menuHelpCheckForUpdates.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpCheckForUpdates_Click);
                // 
                // menuHelpSeparator2
                // 
                // this.menuHelpSeparator2.Enabled = true;
                //
                // menuHelpAbout
                //
                this.menuHelpAbout.Text = "About";
                this.menuHelpAbout.Image = ImageLoader.Get("About.png");
                this.menuHelpAbout.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpAbout_Click);
                //
                // menuHelp
                //
                this.menuHelp.Text = "Help";
                this.menuHelp.Items.Add(this.menuHelpContents);
                this.menuHelp.Items.Add(this.menuHelpIndex);
                this.menuHelp.Items.Add(this.menuHelpOnlineHelp);
                this.menuHelp.Items.Add(this.menuHelpSeparator1);
                this.menuHelp.Items.Add(this.menuHelpLicenceInformation);
                this.menuHelp.Items.Add(this.menuHelpCheckForUpdates);
                this.menuHelp.Items.Add(this.menuHelpSeparator2);
                this.menuHelp.Items.Add(this.menuHelpAbout);
                //
                // menu
                //
                this.menu.Name = "menu";
                this.menu.Items.Add(this.menuFile);
                this.menu.Items.Add(this.menuEdit);
                this.menu.Items.Add(this.menuWindow);
                this.menu.Items.Add(this.menuHelp);
                //
                // buttonFileNew
                //
                this.buttonFileNew.Image = ImageLoader.Get("New.png");
                this.buttonFileNew.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileNew_Click);
                //
                // buttonFileOpen
                //
                this.buttonFileOpen.Image = ImageLoader.Get("Open.png");
                this.buttonFileOpen.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileOpen_Click);
                //
                // buttonFileSave
                //
                this.buttonFileSave.Image = ImageLoader.Get("Save.png");
                this.buttonFileSave.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFileSave_Click);
                //
                // buttonFilePrint
                //
                this.buttonFilePrint.Image = ImageLoader.Get("Print.png");
                this.buttonFilePrint.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuFilePrint_Click);
                //
                // toolBarSeparator0
                //
                // this.toolBarSeparator0.Enabled = true;
                //
                // buttonEditUndo
                //
                this.buttonEditUndo.Image = ImageLoader.Get("Undo.png");
                this.buttonEditUndo.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditUndo_Click);
                //
                // buttonEditRedo
                //
                this.buttonEditRedo.Image = ImageLoader.Get("Redo.png");
                this.buttonEditRedo.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditRedo_Click);
                //
                // buttonEditCut
                //
                this.buttonEditCut.Image = ImageLoader.Get("Cut.png");
                this.buttonEditCut.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditCut_Click);
                //
                // buttonEditCopy
                //
                this.buttonEditCopy.Image = ImageLoader.Get("Copy.png");
                this.buttonEditCopy.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditCopy_Click);
                //
                // buttonEditPaste
                //
                this.buttonEditPaste.Image = ImageLoader.Get("Paste.png");
                this.buttonEditPaste.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditPaste_Click);
                //
                // buttonEditDelete
                //
                this.buttonEditDelete.Image = ImageLoader.Get("Delete.png");
                this.buttonEditDelete.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditDelete_Click);
                //
                // buttonEditFind
                //
                this.buttonEditFind.Image = ImageLoader.Get("Find.png");
                this.buttonEditFind.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditFind_Click);
                //
                // buttonEditReplace
                //
                this.buttonEditReplace.Image = ImageLoader.Get("Replace.png");
                this.buttonEditReplace.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditReplace_Click);
                //
                // buttonEditRefresh
                //
                this.buttonEditRefresh.Image = ImageLoader.Get("Reload.png");
                this.buttonEditRefresh.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditRefresh_Click);
                //
                // buttonEditPreferences
                //
                this.buttonEditPreferences.Image = ImageLoader.Get("Preferences.png");
                this.buttonEditPreferences.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditPreferences_Click);
                //
                // buttonEditProperties
                //
                this.buttonEditProperties.Image = ImageLoader.Get("Properties.png");
                this.buttonEditProperties.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuEditProperties_Click);
                //
                // toolBarSeparator1
                //
                // this.toolBarSeparator1.Enabled = true;
                //
                // buttonHelpContents
                //
                this.buttonHelpContents.Image = ImageLoader.Get("Contents.png");
                this.buttonHelpContents.Click += new System.EventHandler<Modern.Forms.MouseEventArgs>(this.menuHelpContents_Click);
                //
                // toolbar
                //
                this.toolBar.Name = "toolbar";
                Modern.Forms.ToolBar.DefaultStyle.Border.Width = 1;
                this.statusBar.Dock = Modern.Forms.DockStyle.Top;
                this.toolBar.Items.Add(this.buttonFileNew);
                this.toolBar.Items.Add(this.buttonFileOpen);
                this.toolBar.Items.Add(this.buttonFileSave);
                this.toolBar.Items.Add(this.buttonFilePrint);
                this.toolBar.Items.Add(this.toolBarSeparator0);
                this.toolBar.Items.Add(this.buttonEditUndo);
                this.toolBar.Items.Add(this.buttonEditRedo);
                this.toolBar.Items.Add(this.buttonEditCut);
                this.toolBar.Items.Add(this.buttonEditCopy);
                this.toolBar.Items.Add(this.buttonEditPaste);
                this.toolBar.Items.Add(this.buttonEditDelete);
                this.toolBar.Items.Add(this.buttonEditFind);
                this.toolBar.Items.Add(this.buttonEditReplace);
                this.toolBar.Items.Add(this.buttonEditRefresh);
                this.toolBar.Items.Add(this.buttonEditPreferences);
                this.toolBar.Items.Add(this.buttonEditProperties);
                this.toolBar.Items.Add(this.toolBarSeparator1);
                this.toolBar.Items.Add(this.buttonHelpContents);
                // 
                // StatusMessage
                // 
                this.StatusMessage.AutoSize = true;
                this.StatusMessage.Style.ForegroundColor = SkiaSharp.SKColors.Green;
                this.StatusMessage.Name = "StatusMessage";
                this.StatusMessage.Dock = Modern.Forms.DockStyle.Left;
                this.StatusMessage.Height = 20;
                this.StatusMessage.Width = (int)(this.Size.Width * 0.2);
                this.StatusMessage.TextAlign = Modern.Forms.ContentAlignment.MiddleLeft;
                this.StatusMessage.Text = "";
                this.StatusMessage.Click += new EventHandler<Modern.Forms.MouseEventArgs>(this.StatusLabelMessage_Click);
				this.StatusMessage.Margin = new Modern.Forms.Padding(5, 0, 5, 0);
                // 
                // ErrorMessage
                // 
                this.ErrorMessage.AutoSize = true;
                this.ErrorMessage.Style.ForegroundColor = SkiaSharp.SKColors.Red;
                this.ErrorMessage.Name = "ErrorMessage";
                this.ErrorMessage.Dock = Modern.Forms.DockStyle.Left;
                this.ErrorMessage.Height = 20;
                this.ErrorMessage.Width = (int)(this.Size.Width * 0.2);
                this.ErrorMessage.Left = (int)(this.Size.Width * 0.25);
                this.ErrorMessage.TextAlign = Modern.Forms.ContentAlignment.MiddleLeft;
                this.ErrorMessage.Text = "";
                this.ErrorMessage.Click += new EventHandler<Modern.Forms.MouseEventArgs>(this.StatusLabelMessage_Click);
				this.ErrorMessage.Margin = new Modern.Forms.Padding(5, 0, 5, 0);
                // 
                // ProgressBar
                // 
                this.ProgressBar.Dock = Modern.Forms.DockStyle.Right;
                this.ProgressBar.Name = "ProgressBar";
                this.ProgressBar.Size = new System.Drawing.Size(100, 16);
                this.ProgressBar.Value = 10;
                this.ProgressBar.Visible = false;
				this.ProgressBar.Margin = new Modern.Forms.Padding(5, 0, 5, 0);
                // 
                // ActionIcon
                // 
                this.ActionIcon.Dock = Modern.Forms.DockStyle.Right;
                this.ActionIcon.Image = ImageLoader.Get("New.png");
                this.ActionIcon.Name = "ActionIcon";
                this.ActionIcon.Size = new System.Drawing.Size(16, 17);
                this.ActionIcon.Visible = false;
				this.ActionIcon.Margin = new Modern.Forms.Padding(5, 0, 5, 0);
                // 
                // DirtyIcon
                // 
                this.DirtyIcon.Dock = Modern.Forms.DockStyle.Right;
                this.DirtyIcon.Image = ImageLoader.Get("Save.png");
                this.DirtyIcon.Name = "DirtyIcon";
                this.DirtyIcon.Size = new System.Drawing.Size(16, 17);
                this.DirtyIcon.Visible = false;
				this.DirtyIcon.Margin = new Modern.Forms.Padding(5, 0, 5, 0);
				//
				// statusBar
				//
				Modern.Forms.Panel.DefaultStyle.Border.Width = 1;
                this.statusBar.Controls.Add(this.ProgressBar);
                this.statusBar.Controls.Add(this.ActionIcon);
                this.statusBar.Controls.Add(this.DirtyIcon);
                this.statusBar.Controls.Add(this.ErrorMessage);
                this.statusBar.Controls.Add(this.StatusMessage);
                this.statusBar.Dock = Modern.Forms.DockStyle.Bottom;
                this.statusBar.Height = 25;
                this.statusBar.Name = "statusBar";
                this.statusBar.TabIndex = 22;
                this.statusBar.Text = "statusBar";
                // 
                // lblSomeInt
                // 
                this.lblSomeInt.AutoSize = true;
                this.lblSomeInt.Location = new System.Drawing.Point(5, 115);
                this.lblSomeInt.Name = "lblSomeInt";
                this.lblSomeInt.TabIndex = 119;
                this.lblSomeInt.Text = "Int:";
                // 
                // lblSomeString
                // 
                this.lblSomeString.AutoSize = true;
                this.lblSomeString.Location = new System.Drawing.Point(5, 135);
                this.lblSomeString.Name = "lblSomeString";
                this.lblSomeString.TabIndex = 120;
                this.lblSomeString.Text = "String:";
                //
                // lblSomeBoolean
                //
                this.lblSomeBoolean.AutoSize = true;
                this.lblSomeBoolean.Location = new System.Drawing.Point(5, 155);
                this.lblSomeBoolean.Name = "lblSomeBoolean";
                this.lblSomeBoolean.TabIndex = 120;
                this.lblSomeBoolean.Text = "Bool:";
                // 
                // txtSomeInt
                // 
                this.txtSomeInt.Location = new System.Drawing.Point(60, 110);
                this.txtSomeInt.Name = "txtSomeInt";
                this.txtSomeInt.Size = new System.Drawing.Size(50, 20);
                this.txtSomeInt.TabIndex = 122;
                // 
                // txtSomeString
                // 
                this.txtSomeString.Location = new System.Drawing.Point(60, 130);
                this.txtSomeString.Name = "txtSomeString";
                this.txtSomeString.Size = new System.Drawing.Size(100, 20);
                this.txtSomeString.TabIndex = 123;
                // 
                // chkSomeBoolean
                // 
                this.chkSomeBoolean.AutoSize = true;
                this.chkSomeBoolean.Location = new System.Drawing.Point(60, 155);
                this.chkSomeBoolean.Name = "chkSomeBoolean";
                this.chkSomeBoolean.Size = new System.Drawing.Size(65, 17);
                this.chkSomeBoolean.TabIndex = 124;
                this.chkSomeBoolean.Text = "";
                // 
                // cmdRun
                // 
                this.cmdRun.Location = new System.Drawing.Point(552, 185);
                this.cmdRun.Name = "cmdRun";
                this.cmdRun.Size = new System.Drawing.Size(75, 23);
                this.cmdRun.TabIndex = 125;
                this.cmdRun.Text = "Run";
                this.cmdRun.Click += new EventHandler<Modern.Forms.MouseEventArgs>(this.cmdRun_Click);
                //
                // cmdFont
                //
                this.cmdFont.Location = new System.Drawing.Point(552, 210);
                this.cmdFont.Name = "cmdFont";
                this.cmdFont.Size = new System.Drawing.Size(75, 23);
                this.cmdFont.TabIndex = 126;
                this.cmdFont.Text = "Font";
                this.cmdFont.Click += new EventHandler<Modern.Forms.MouseEventArgs>(this.cmdFont_Click);
                //
                // cmdColor
                //
                this.cmdColor.Location = new System.Drawing.Point(552, 235);
                this.cmdColor.Name = "cmdColor";
                this.cmdColor.Size = new System.Drawing.Size(75, 23);
                this.cmdColor.TabIndex = 127;
                this.cmdColor.Text = "Color";
                this.cmdColor.Click += new EventHandler<Modern.Forms.MouseEventArgs>(this.cmdColor_Click);
                // 
                // lblSomeOtherInt
                // 
                this.lblSomeOtherInt.AutoSize = true;
                this.lblSomeOtherInt.Location = new System.Drawing.Point(180, 115);
                this.lblSomeOtherInt.Name = "lblSomeOtherInt";
                this.lblSomeOtherInt.TabIndex = 126;
                this.lblSomeOtherInt.Text = "Other Int:";
                // 
                // lblSomeOtherString
                // 
                this.lblSomeOtherString.AutoSize = true;
                this.lblSomeOtherString.Location = new System.Drawing.Point(180, 135);
                this.lblSomeOtherString.Name = "lblSomeOtherString";
                this.lblSomeOtherString.TabIndex = 127;
                this.lblSomeOtherString.Text = "Other String:";
                //
                // lblSomeOtherBoolean
                //
                this.lblSomeOtherBoolean.AutoSize = true;
                this.lblSomeOtherBoolean.Location = new System.Drawing.Point(180, 155);
                this.lblSomeOtherBoolean.Name = "lblSomeOtherBoolean";
                this.lblSomeOtherBoolean.TabIndex = 127;
                this.lblSomeOtherBoolean.Text = "Other Bool:";
                // 
                // txtSomeOtherInt
                // 
                this.txtSomeOtherInt.Location = new System.Drawing.Point(282, 110);
                this.txtSomeOtherInt.Name = "txtSomeOtherInt";
                this.txtSomeOtherInt.Size = new System.Drawing.Size(50, 20);
                this.txtSomeOtherInt.TabIndex = 128;
                // 
                // txtSomeOtherString
                // 
                this.txtSomeOtherString.Location = new System.Drawing.Point(282, 130);
                this.txtSomeOtherString.Name = "txtSomeOtherString";
                this.txtSomeOtherString.Size = new System.Drawing.Size(100, 20);
                this.txtSomeOtherString.TabIndex = 129;
                // 
                // chkSomeOtherBoolean
                // 
                this.chkSomeOtherBoolean.AutoSize = true;
                this.chkSomeOtherBoolean.Location = new System.Drawing.Point(282, 155);
                this.chkSomeOtherBoolean.Name = "chkSomeOtherBoolean";
                this.chkSomeOtherBoolean.Size = new System.Drawing.Size(65, 17);
                this.chkSomeOtherBoolean.TabIndex = 130;
                this.chkSomeOtherBoolean.Text = "";
                // 
                // lblStillAnotherInt
                // 
                this.lblStillAnotherInt.AutoSize = true;
                this.lblStillAnotherInt.Location = new System.Drawing.Point(395, 115);
                this.lblStillAnotherInt.Name = "lblStillAnotherInt";
                this.lblStillAnotherInt.TabIndex = 131;
                this.lblStillAnotherInt.Text = "Another Int:";
                // 
                // lblStillAnotherString
                // 
                this.lblStillAnotherString.AutoSize = true;
                this.lblStillAnotherString.Location = new System.Drawing.Point(395, 135);
                this.lblStillAnotherString.Name = "lblStillAnotherString";
                this.lblStillAnotherString.TabIndex = 132;
                this.lblStillAnotherString.Text = "Another String:";
                //
                // lblStillAnotherBoolean
                //
                this.lblStillAnotherBoolean.AutoSize = true;
                this.lblStillAnotherBoolean.Location = new System.Drawing.Point(395, 155);
                this.lblStillAnotherBoolean.Name = "lblStillAnotherBoolean";
                this.lblStillAnotherBoolean.TabIndex = 132;
                this.lblStillAnotherBoolean.Text = "Another Bool:";
                // 
                // txtStillAnotherInt
                // 
                this.txtStillAnotherInt.Location = new System.Drawing.Point(505, 110);
                this.txtStillAnotherInt.Name = "txtStillAnotherInt";
                this.txtStillAnotherInt.Size = new System.Drawing.Size(50, 20);
                this.txtStillAnotherInt.TabIndex = 133;
                // 
                // txtStillAnotherString
                // 
                this.txtStillAnotherString.Location = new System.Drawing.Point(505, 130);
                this.txtStillAnotherString.Name = "txtStillAnotherString";
                this.txtStillAnotherString.Size = new System.Drawing.Size(100, 20);
                this.txtStillAnotherString.TabIndex = 134;
                // 
                // chkStillAnotherBoolean
                // 
                this.chkStillAnotherBoolean.AutoSize = true;
                this.chkStillAnotherBoolean.Location = new System.Drawing.Point(505, 155);
                this.chkStillAnotherBoolean.Name = "chkStillAnotherBoolean";
                this.chkStillAnotherBoolean.Size = new System.Drawing.Size(65, 17);
                this.chkStillAnotherBoolean.TabIndex = 135;
                this.chkStillAnotherBoolean.Text = "";
                // 
                // MvcView
                // 
                this.Controls.Add(this.toolBar);
                this.Controls.Add(this.menu);
                this.Controls.Add(this.statusBar);
                this.Controls.Add(this.lblSomeInt);
                this.Controls.Add(this.lblSomeString);
                this.Controls.Add(this.lblSomeBoolean);
                this.Controls.Add(this.txtSomeInt);
                this.Controls.Add(this.txtSomeString);
                this.Controls.Add(this.chkSomeBoolean);
                this.Controls.Add(this.lblSomeOtherInt);
                this.Controls.Add(this.lblSomeOtherString);
                this.Controls.Add(this.lblSomeOtherBoolean);
                this.Controls.Add(this.txtSomeOtherInt);
                this.Controls.Add(this.txtSomeOtherString);
                this.Controls.Add(this.chkSomeOtherBoolean);
                this.Controls.Add(this.lblStillAnotherInt);
                this.Controls.Add(this.lblStillAnotherString);
                this.Controls.Add(this.lblStillAnotherBoolean);
                this.Controls.Add(this.txtStillAnotherInt);
                this.Controls.Add(this.txtStillAnotherString);
                this.Controls.Add(this.chkStillAnotherBoolean);
                this.Controls.Add(this.cmdRun);
                this.Controls.Add(this.cmdFont);
                this.Controls.Add(this.cmdColor);
                this.Image = ImageLoader.Get("App.png");
                this.Text = "MvcForms.Core.ModernForms";
                this.Shown += new System.EventHandler(this.View_Shown);
                this.Closing += new System.EventHandler<System.ComponentModel.CancelEventArgs>(this.View_Closing);
                this.Size = new System.Drawing.Size(800, 600);
                this.menu.ResumeLayout(false);
                this.menu.PerformLayout();
                // this.toolbar.ResumeLayout(false);
                // this.toolbar.PerformLayout();
                this.statusBar.ResumeLayout(false);
                this.statusBar.PerformLayout();
                // this.ResumeLayout(false);
                // this.PerformLayout();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion

        private Modern.Forms.Menu menu;
        private Modern.Forms.MenuItem menuFile;
        private Modern.Forms.MenuItem menuFileNew;
        private Modern.Forms.MenuItem menuFileOpen;
        private Modern.Forms.MenuItem menuFileSave;
        private Modern.Forms.MenuItem menuFileSaveAs;
        private Modern.Forms.MenuSeparatorItem menuFileSeparator1;
        private Modern.Forms.MenuItem menuFilePrint;
        private Modern.Forms.MenuItem menuFilePrintPreview;
        private Modern.Forms.MenuSeparatorItem menuFileSeparator2;
        private Modern.Forms.MenuItem menuFileExit;
        private Modern.Forms.MenuItem menuEdit;
        private Modern.Forms.MenuItem menuEditUndo;
        private Modern.Forms.MenuItem menuEditRedo;
        private Modern.Forms.MenuSeparatorItem menuEditSeparator0;
        private Modern.Forms.MenuItem menuEditSelectAll;
        private Modern.Forms.MenuItem menuEditCut;
        private Modern.Forms.MenuItem menuEditCopy;
        private Modern.Forms.MenuItem menuEditPaste;
        private Modern.Forms.MenuItem menuEditPasteSpecial;
        private Modern.Forms.MenuItem menuEditDelete;
        private Modern.Forms.MenuSeparatorItem menuEditSeparator1;
        private Modern.Forms.MenuItem menuEditFind;
        private Modern.Forms.MenuItem menuEditReplace;
        private Modern.Forms.MenuSeparatorItem menuEditSeparator2;
        private Modern.Forms.MenuItem menuEditRefresh;
        private Modern.Forms.MenuSeparatorItem menuEditSeparator3;
        private Modern.Forms.MenuItem menuEditPreferences;
        private Modern.Forms.MenuItem menuEditProperties;
        private Modern.Forms.MenuItem menuWindow;
        private Modern.Forms.MenuItem menuWindowNewWindow;
        private Modern.Forms.MenuItem menuWindowTile;
        private Modern.Forms.MenuItem menuWindowCascade;
        private Modern.Forms.MenuItem menuWindowArrangeAll;
        private Modern.Forms.MenuSeparatorItem menuWindowSeparator0;
        private Modern.Forms.MenuItem menuWindowHide;
        private Modern.Forms.MenuItem menuWindowShow;
        private Modern.Forms.MenuItem menuHelp;
        private Modern.Forms.MenuItem menuHelpContents;
        private Modern.Forms.MenuItem menuHelpIndex;
        private Modern.Forms.MenuItem menuHelpOnlineHelp;
        private Modern.Forms.MenuSeparatorItem menuHelpSeparator1;
        private Modern.Forms.MenuItem menuHelpLicenceInformation;
        private Modern.Forms.MenuItem menuHelpCheckForUpdates;
        private Modern.Forms.MenuSeparatorItem menuHelpSeparator2;
        private Modern.Forms.MenuItem menuHelpAbout;
        private Modern.Forms.ToolBar toolBar;
        private Modern.Forms.MenuItem buttonFileNew;
        private Modern.Forms.MenuItem buttonFileOpen;
        private Modern.Forms.MenuItem buttonFileSave;
        private Modern.Forms.MenuItem buttonFilePrint;
        private Modern.Forms.MenuSeparatorItem toolBarSeparator0;
        private Modern.Forms.MenuItem buttonEditUndo;
        private Modern.Forms.MenuItem buttonEditRedo;
        private Modern.Forms.MenuItem buttonEditCut;
        private Modern.Forms.MenuItem buttonEditCopy;
        private Modern.Forms.MenuItem buttonEditPaste;
        private Modern.Forms.MenuItem buttonEditDelete;
        private Modern.Forms.MenuItem buttonEditFind;
        private Modern.Forms.MenuItem buttonEditReplace;
        private Modern.Forms.MenuItem buttonEditRefresh;
        private Modern.Forms.MenuItem buttonEditPreferences;
        private Modern.Forms.MenuItem buttonEditProperties;
        private Modern.Forms.MenuSeparatorItem toolBarSeparator1;
        private Modern.Forms.MenuItem buttonHelpContents;
        private Modern.Forms.Panel statusBar;
        private Modern.Forms.Label StatusMessage;
        private Modern.Forms.Label ErrorMessage;
        private Modern.Forms.ProgressBar ProgressBar;
        private Modern.Forms.PictureBox ActionIcon;
        private Modern.Forms.PictureBox DirtyIcon;
        private Modern.Forms.Button cmdRun;
        private Modern.Forms.Button cmdFont;
        private Modern.Forms.Button cmdColor;
        private Modern.Forms.Label lblSomeInt;
        private Modern.Forms.Label lblSomeString;
        private Modern.Forms.Label lblSomeBoolean;
        private Modern.Forms.TextBox txtSomeInt;
        private Modern.Forms.TextBox txtSomeString;
        private Modern.Forms.CheckBox chkSomeBoolean;
        private Modern.Forms.Label lblSomeOtherInt;
        private Modern.Forms.Label lblSomeOtherString;
        private Modern.Forms.Label lblSomeOtherBoolean;
        private Modern.Forms.TextBox txtSomeOtherInt;
        private Modern.Forms.TextBox txtSomeOtherString;
        private Modern.Forms.CheckBox chkSomeOtherBoolean;
        private Modern.Forms.Label lblStillAnotherInt;
        private Modern.Forms.Label lblStillAnotherString;
        private Modern.Forms.Label lblStillAnotherBoolean;
        private Modern.Forms.TextBox txtStillAnotherInt;
        private Modern.Forms.TextBox txtStillAnotherString;
        private Modern.Forms.CheckBox chkStillAnotherBoolean;

    }
}
