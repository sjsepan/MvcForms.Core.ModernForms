﻿#pragma warning disable IDE1006 //naming conventions
#pragma warning disable IDE1005 //simplify delegate invocation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.ModernForms;
using MvcLibrary.Core;
using Modern.Forms;
using SkiaSharp;

namespace MvcForms.Core.ModernForms
{
    public partial class MvcView :
        Form,
        INotifyPropertyChanged
    {
        #region Declarations
        private bool isHandlingClose;
        private bool isHandlingDisposeSave;

		//cancellation hook
		// Action cancelDelegate;
        protected MVCViewModel ViewModel;

		#endregion Declarations

		#region Constructors
		public MvcView()
        {
            try
            {
                InitializeComponent();

                StatusMessage.Text = "";
                ErrorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

				// BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        #endregion Properties

        #region INotifyPropertyChanged
        public /*new*/ event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Events

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    StatusMessage.Text = (ViewModel?.StatusMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after ViewModel is null
                    ErrorMessage.Text = (ViewModel?.ErrorMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    ErrorMessage.Tag = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    ProgressBar.Value = ViewModel.ProgressBarValue;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    ProgressBar.Maximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    ProgressBar.Minimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    ProgressBar.Step = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    // ProgressBar.Marquee = ViewModel.ProgressBarIsMarquee;
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    ProgressBar.Visible = ViewModel.ProgressBarIsVisible;
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    DirtyIcon.Visible = ViewModel.DirtyIconIsVisible;
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    DirtyIcon.Image = ViewModel.DirtyIconImage;
                }
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    ActionIcon.Visible = ViewModel.ActionIconIsVisible;
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    ActionIcon.Image = (ViewModel?.ActionIconImage);
                }

                //Fields
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   chkSomeBoolean.Checked = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   chkStillAnotherBoolean.Checked = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   chkSomeOtherBoolean.Checked = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form
        private void View_Shown(object sender, EventArgs e)
        {//Note: do only once
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                //_Run();
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private async void View_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                //Debug:Closing event is NOT awaiting the result from DisposeSettings;
                // cancel Close long enough for dialog to be shown.
                //Make a static that can be set below, and checked above...
                if (isHandlingClose)
                {
                    if (isHandlingDisposeSave)
                    {
                        // fall out with Cancel = false
                    }
                    else
                    {
                        //cancel because we haven't asked user about Save yet
                        isHandlingDisposeSave = true;
                        e.Cancel = true;

                        //clean up data model here
                        ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                        await DisposeSettings();
                        ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                        ViewModel = null;

                        //trigger next stage of Close
                        Close();
                    }
                }
                else
                {
                    //cancel because we haven't asked user about Close yet
                    isHandlingClose = true;
                    e.Cancel = true;

                    //trigger FileQuitAction to ask user
                    if (await ViewModel.FileExit())
                    {
                        // fall out with Cancel = true

                        //reset flags for later
                        isHandlingClose = false;
					}
                    else
                    {
                        //trigger next stage of Close
                        Close();
                    }
				}
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("View_Closing" + ex.Message);
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Form

        #region Controls
        private async void StatusLabelMessage_Click(object sender, EventArgs e)
        {
			try
			{
				DialogBoxForm messageBoxForm = new(DialogBoxForm.ButtonTypes.OK, ImageLoader.Get("Info.ico"));
				messageBoxForm.TitleBar.Text = (sender as Label).Name;
                messageBoxForm.Message = (sender as Label).Text;

				DialogResult response = await messageBoxForm.ShowDialog(this);

				if (response == DialogResult.OK)
                {
                    //Do nothing
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void cmdFont_Click(object sender, EventArgs e)
        {
            try
            {
                ViewModel.GetFont();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void cmdColor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewModel.GetColor();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void cmdRun_Click(object sender, EventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }
        #endregion Controls

        #region Menu
        private async void menuFileNew_Click(object sender, EventArgs e)
        {
            await ViewModel.FileNew();
        }

        private async void menuFileOpen_Click(object sender, EventArgs e)
        {
            await ViewModel.FileOpen();
        }

        private async void menuFileSave_Click(object sender, EventArgs e)
        {
            await ViewModel.FileSave();
        }

        private async void menuFileSaveAs_Click(object sender, EventArgs e)
        {
            await ViewModel.FileSaveAs();
        }

        private async void menuFilePrint_Click(object sender, EventArgs e)
        {
            await ViewModel.FilePrint();
        }

        private async void menuFilePrintPreview_Click(object sender, EventArgs e)
        {
            await ViewModel.FilePrintPreview();
        }

        private void menuFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void menuEditUndo_Click(object sender, EventArgs e)
        {
             await ViewModel.EditUndo();
        }

        private async void menuEditRedo_Click(object sender, EventArgs e)
        {
            await ViewModel.EditRedo();
        }

        private async void menuEditSelectAll_Click(object sender, EventArgs e)
        {
            await ViewModel.EditSelectAll();
        }

        private async void menuEditCut_Click(object sender, EventArgs e)
        {
            await ViewModel.EditCut();
        }

        private async void menuEditCopy_Click(object sender, EventArgs e)
        {
            await ViewModel.EditCopy();
        }

        private async void menuEditPaste_Click(object sender, EventArgs e)
        {
            await ViewModel.EditPaste();
        }

        private async void menuEditPasteSpecial_Click(object sender, EventArgs e)
        {
            await ViewModel.EditPasteSpecial();
        }

        private async void menuEditDelete_Click(object sender, EventArgs e)
        {
            await ViewModel.EditDelete();
        }

        private async void menuEditFind_Click(object sender, EventArgs e)
        {
            await ViewModel.EditFind();
        }

        private async void menuEditReplace_Click(object sender, EventArgs e)
        {
            await ViewModel.EditReplace();
        }

        private async void menuEditRefresh_Click(object sender, EventArgs e)
        {
            await ViewModel.EditRefresh();
        }

        private async void menuEditPreferences_Click(object sender, EventArgs e)
        {
            await ViewModel.EditPreferences();
        }

        private async void menuEditProperties_Click(object sender, EventArgs e)
        {
			await ViewModel.EditProperties();
		}

        private async void menuWindowNewWindow_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowNewWindow();
        }

        private async void menuWindowTile_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowTile();
        }

        private async void menuWindowCascade_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowCascade();
        }

        private async void menuWindowArrangeAll_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowArrangeAll();
        }

        private async void menuWindowHide_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowHide();
        }

        private async void menuWindowShow_Click(object sender, EventArgs e)
        {
            await ViewModel.WindowShow();
        }

        private async void menuHelpContents_Click(object sender, EventArgs e)
        {
            await ViewModel.HelpContents();
        }

        private async void menuHelpIndex_Click(object sender, EventArgs e)
        {
            await ViewModel.HelpIndex();
        }

        private async void menuHelpOnlineHelp_Click(object sender, EventArgs e)
        {
            await ViewModel.HelpOnlineHelp();
        }

        private async void menuHelpLicenceInformation_Click(object sender, EventArgs e)
        {
            await ViewModel.HelpLicenceInformation();
        }

        private async void menuHelpCheckForUpdates_Click(object sender, EventArgs e)
        {
            await ViewModel.HelpCheckForUpdates();
        }

        private void menuHelpAbout_Click(object sender, EventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu

        #region Toolbar Events
        //Note: toolbar buttons using same click event as corresponding menu items
        #endregion Toolbar Events

        #endregion Events

        #region Methods

        #region Actions
        // private async Task DoSomething()
        // {
        //     for (int i = 0; i < 3; i++)
        //     {
        //         //StatusBarProgressBar.Pulse();
        //         //DoEvents;
        //         await Task.Delay(1000);
        //     }
        // }

//TODO:PrintPreview
//TODO:PasteSpecial
        #endregion Actions

        #region FormAppBase
        protected async void InitViewModel()
        {
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();
				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<Form, DialogResult>
					(
						parent: this,
						modal: true,
						title: null,
						response: DialogResult.None,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters: [
							"MvcSettings files (*.mvcsettings)|mvcsettings",
							"JSON files (*.json)|json",
							"XML files (*.xml)|xml",
							"All files (*.*)|*"
						],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator(),
						selectFolders: false
					)
					{
						//set dialog caption
						Title = TitleBar.Text
					};

				//class to handle standard behaviors
				ViewModelController<SKBitmap, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, SKBitmap>()
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "New", ImageLoader.Get("New.png") },
                            { "Open", ImageLoader.Get("Open.png") },
                            { "Save", ImageLoader.Get("Save.png") },
                            { "SaveAs", ImageLoader.Get("Save.png") },
                            { "Print", ImageLoader.Get("Print.png") },
                            { "PrintPreview", ImageLoader.Get("Print.png") },
                            { "Undo", ImageLoader.Get("Undo.png") },
                            { "Redo", ImageLoader.Get("Redo.png") },
                            { "Cut", ImageLoader.Get("Cut.png") },
                            { "Copy", ImageLoader.Get("Copy.png") },
                            { "Paste", ImageLoader.Get("Paste.png") },
                            { "Delete", ImageLoader.Get("Delete.png") },
                            { "Find", ImageLoader.Get("Find.png") },
                            { "Replace", ImageLoader.Get("Replace.png") },
                            { "Refresh", ImageLoader.Get("Reload.png") },
                            { "Preferences", ImageLoader.Get("Preferences.png") },
                            { "Properties", ImageLoader.Get("Properties.png") },
                            { "Contents", ImageLoader.Get("Contents.png") },
                            { "About", ImageLoader.Get("About.png") }
                        },
                        settingsFileDialogInfo,
                        this//use ctor overload that takes view
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<SKBitmap, MVCViewModel>.ViewModel[ViewName];

				BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    await ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    await ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
			try
			{
				//create Settings before first use by Model
				if (SettingsController<MVCSettings>.Settings == null)
				{
					SettingsController<MVCSettings>.New();
				}
				//Model properties rely on Settings, so don't call Refresh before this is run.
				if (ModelController<MVCModel>.Model == null)
				{
					ModelController<MVCModel>.New();
				}
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
		}

        protected async Task DisposeSettings()
        {
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> questionMessageDialogInfo = null;
            string errorMessage = null;

            //save user and application settings 
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
				//prompt before saving
				questionMessageDialogInfo =
                    new MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes>
                    (
                        this,
                        true,
                        "Save",
                        null,
                        ImageLoader.Get("Question.png"),
                        DialogBoxForm.ButtonTypes.YesNo,
                        "Save changes?",
                        DialogResult.None
                    );

                questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);
                if (!questionMessageDialogInfo.BoolResult)
                {
                    errorMessage = questionMessageDialogInfo.ErrorMessage;
                    throw new ApplicationException(errorMessage);
                }

				switch (questionMessageDialogInfo.Response)
                {
					case DialogResult.Yes:
						//SAVE
						await ViewModel.FileSave();

						break;

					case DialogResult.No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        protected static void _Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        /// <summary>
        /// Bind static Model controls to Model Controller
        /// </summary>
        private static void BindFormUi()
        {
            try
            {
                //Form

                //Controls
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Bind Model controls to Model Controller
        /// Note: databinding not available in AvaloniaUI, but leave this in place
        ///  in case I figure out a way to do this
        /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
		// 		BindField(txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
		// 		BindField(txtSomeString, ModelController<MVCModel>.Model, "SomeString");
		// 		BindField(chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

		// 		BindField(txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
		// 		BindField(txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
		// 		BindField(chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");

		// 		BindField(txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
		// 		BindField(txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
		// 		BindField(chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        /// <summary>
        /// TODO: databinding different in AvaloniaUI, but leave this in place
        ///  in case I figure out how to use this
        /// </summary>
        // private static void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     where TControl : Control
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO:.AddSignalHandler ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                this.TitleBar.Text = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = (SettingsController<MVCSettings>.Settings.Dirty);
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic
        // ///  to be triggered by refresh?
        // /// </summary>
        // /// <param name="functionButton"></param>
        // /// <param name="functionMenu"></param>
        // /// <param name="cancelButton"></param>
        // /// <param name="enable"></param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     Button functionToolbarButton,
        //     MenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.Enabled = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.Enabled = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             functionMenu.Enabled = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.Enabled = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Invoke any delegate that has been registered
        ///  to cancel a long-running background process.
        /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;
			try
			{
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

				returnValue = true;
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				//throw;
			}
			return returnValue;
        }

        // private static void BindSizeAndLocation()
        // {

        //     //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
        //     // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        //     // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        //     //Note:Size is readonly
        //     //Size.Height = Properties.Settings.Default.Size.Height;
        //     //Size.Width = Properties.Settings.Default.Size.Width;
        //     //TODO:this.Position.X =  global::MvcForms.Core.ModernForms.Properties.Settings.Default.Location.X;
        //     //TODO:this.Position.Y =  global::MvcForms.Core.ModernForms.Properties.Settings.Default.Location.Y;
        // }

//TODO:use equivalents from ViewModel
        // private void SetStatusMessage(string message)
        // {
        //     try
        //     {
        //         this.StatusMessage.Text = message;
		// 		// this.StatusMessage.ToolTip = message;
		// 	}
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // private string GetStatusMessage()
        // {
        //     string message = null;
        //     try
        //     {
        //         message = this.StatusMessage.Text;
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }

        //     return message;
        // }
        // private void SetErrorMessage(string message)
        // {
        //     try
        //     {
        //         this.ErrorMessage.Text = message;
		// 		// this.ErrorMessage.ToolTip = message;
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }
        // private void StartProgressBar()
        // {
        //     ProgressBar.Value = 33;
        //     // ProgressBar.Style = ProgressBarStyle.Marquee;//true;
        //     ProgressBar.Visible = true;
        //     //DoEvents;
        // }

        // private void StopProgressBar()
        // {
        //     //DoEvents;
        //     ProgressBar.Visible = false;
        // }

        // private void StartActionIcon(SkiaSharp.SKBitmap resourceItem)
        // {
        //     ActionIcon.Image = (resourceItem == null ? null : resourceItem);
        //     ActionIcon.Visible = true;
        // }

        // private void StopActionIcon()
        // {
        //     ActionIcon.Visible = false;
        //     ActionIcon.Image = ImageLoader.Get("New.png"); 
        // }

        #endregion Utility
        #endregion Methods
    }
}
