# Desktop GUI app demo, on Linux, in C# / DotNet[5/6] / ModernForms, using VSCode

![MvcForms.Core.ModernForms.png](./MvcForms.Core.ModernForms.png?raw=true "Screenshot")

## Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

## Instructions/example of adding ModernForms to dotnet

dotnet new --install ModernForms.Templates
dotnet new modernforms
dotnet add package Modern.Forms
dotnet add package Modern.WindowKit

## Issues

~No tooltip on Label controls

## History

0.4:
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

0.3:

~Add App.config; Fix startup handling of settings filename in App.config.

0.2:

~update to .net8.0.

0.1:

~initial release
~using .Net6.0, Modern.Forms 0.3.0 and Modern.WindowKit 0.1.2

## Contact

Steve Sepan
<sjsepan@yahoo.com>
3-6-2024
