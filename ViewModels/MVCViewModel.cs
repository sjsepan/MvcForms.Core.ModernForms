﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.ModernForms;
using MvcLibrary.Core;
using Modern.Forms;
using SkiaSharp;

namespace MvcForms.Core.ModernForms
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel<SKBitmap, MVCSettings, MVCModel, MvcView>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, SKBitmap> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, SKBitmap> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo,
            MvcView view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        private static async Task DoSomethingElse()
        {
            for (int i = 0; i < 3; i++)
            {
                //StatusBarProgressBar.Pulse();
                //DoEvents;
                await Task.Delay(1000);
            }
        }

        public async Task FilePrint()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            PrinterDialogInfo<Form, DialogResult, string> printerDialoginfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Print" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Print"],
                    true,
                    33
                );

                //select printer
                printerDialoginfo = new PrinterDialogInfo<Form, DialogResult, string>
                (
                    View,
                    true,
                    "Select Printer",
                    DialogResult.None
                );

                if (Dialogs.GetPrinter(ref printerDialoginfo, ref errorMessage))
                {
                    // Log.Write(printerDialoginfo.Printer.Name,  Log.EventLogEntryType_Error);
                    if (printerDialoginfo.Response == DialogResult.OK)
                    {
                        StopProgressBar(StatusMessage + printerDialoginfo.Printer + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar(StatusMessage + ACTION_CANCELLED);
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                if (await Print())
                {
                    // StopProgressBar("Printed.");
                }
                else
                {
                    // StopProgressBar("Print cancelled.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Print()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            // finally
            // {
            // }

            return returnValue;
        }

        public async Task FilePrintPreview()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Print Preview" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["PrintPreview"],
                    true,
                    33
                );

                //TODO:select printer?

                if (await PrintPreview())
                {
                    StopProgressBar(StatusMessage + ACTION_DONE);
                }
                else
                {
                    StopProgressBar(StatusMessage + ACTION_CANCELLED);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> PrintPreview()
        {
            bool returnValue = default;
            // string errorMessage = null;
            // PrinterDialogInfo printerDialoginfo = null;

            try
            {
                // if (Dialogs.GetPrinter(ref printerDialoginfo, ref errorMessage))
                // {
                //     Log.Write(printerDialoginfo.Printer.Name,  Log.EventLogEntryType_Error);

                    await DoSomethingElse();
                    returnValue = true;
                // }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            // finally
            // {
            // }

            return returnValue;
        }

        public async Task EditUndo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Undo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Undo"],
                    true,
                    33
                );

                if (!await Undo())
                {
                    throw new ApplicationException("'Undo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Undo()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditRedo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Redo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Redo"],
                    true,
                    33
                );

                if (!await Redo())
                {
                    throw new ApplicationException("'Redo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Redo()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditSelectAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Select All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["SelectAll"],
                    true,
                    33
                );

                if (!await SelectAll())
                {
                    throw new ApplicationException("'Select All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> SelectAll()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditCut()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

			try
            {
                StartProgressBar
                (
                    "Cut" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Cut"],
                    true,
                    33
                );

				if (!await Cut())
                {
                    throw new ApplicationException("'Cut' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        protected static async Task<bool> Cut()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditCopy()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Copy" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Copy"],
                    true,
                    33
                );

                if (!await Copy())
                {
                    throw new ApplicationException("'Copy' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Copy()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditPaste()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Paste"],
                    true,
                    33
                );

                if (!await Paste())
                {
                    throw new ApplicationException("'Paste' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Paste()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditPasteSpecial()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past Special" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["PasteSpecial"],
                    true,
                    33
                );

                if (!await PasteSpecial())
                {
                    throw new ApplicationException("'Paste Special' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> PasteSpecial()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditDelete()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Delete" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Delete"],
                    true,
                    33
                );

                if (!await Delete())
                {
                    throw new ApplicationException("'Delete' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Delete()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditFind()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Find" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Find"],
                    true,
                    33
                );

                if (!await Find())
                {
                    throw new ApplicationException("'Find' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Find()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditReplace()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Replace" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Replace"],
                    true,
                    33
                );

                if (!await Replace())
                {
                    throw new ApplicationException("'Replace' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Replace()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditRefresh()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Refresh" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Refresh"],
                    true,
                    33
                );

                if (!await Refresh())
                {
                    throw new ApplicationException("'Refresh' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Refresh()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditPreferences()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                if (!await Preferences())
                {
                    //throw new ApplicationException("'Preferences' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected async Task<bool> Preferences()
        {
            bool returnValue = default;
            FileDialogInfo<Form, DialogResult> fileDialogInfo = null;
            string errorMessage = null;

            //for preferences, do a folder-path dialog demo 
            try
            {
                fileDialogInfo = new FileDialogInfo<Form, DialogResult>
                (
                    parent: View,
                    modal: true,
                    title:  "Open Folder...",
                    response: DialogResult.None
                );

                fileDialogInfo = await Dialogs.GetFolderPath(fileDialogInfo);

                if (!fileDialogInfo.BoolResult)
                {
                    errorMessage = fileDialogInfo.ErrorMessage;
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != DialogResult.None)
                {
                    if (fileDialogInfo.Response == DialogResult.OK)
                    {
                        SettingsController<MVCSettings>.FilePath = fileDialogInfo.Filename;
						UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                        returnValue = true;
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS);
                        returnValue = true;
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS);
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
				fileDialogInfo.ErrorMessage = ex.Message;
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task EditProperties()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Properties" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Properties"],
                    true,
                    33
                );

                if (!await Properties())
                {
                    throw new ApplicationException("'Properties' error");
                }

                //TODO:when implemented, a Properties dialog can also be cancelled, so handle cancel too
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Properties()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowNewWindow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "New Window" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["NewWindow"],
                    true,
                    33
                );

                if (!await NewWindow())
                {
                    throw new ApplicationException("'New Window' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> NewWindow()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowTile()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Tile" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Tile"],
                    true,
                    33
                );

                if (!await Tile())
                {
                    throw new ApplicationException("'Tile' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Tile()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowCascade()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Cascade" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Cascade"],
                    true,
                    33
                );

                if (!await Cascade())
                {
                    throw new ApplicationException("'Cascade' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Cascade()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowArrangeAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Arrange All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["ArrangeAll"],
                    true,
                    33
                );

                if (!await ArrangeAll())
                {
                    throw new ApplicationException("'Arrange All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> ArrangeAll()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowHide()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Hide" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Hide"],
                    true,
                    33
                );

                if (!await Hide())
                {
                    throw new ApplicationException("'Hide' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Hide()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task WindowShow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Show" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Show"],
                    true,
                    33
                );

                if (!await Show())
                {
                    throw new ApplicationException("'Show' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Show()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task HelpContents()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Contents" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Contents"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!await Contents())
                {
                    throw new ApplicationException("'Help Contents' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Contents()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task HelpIndex()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Index" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Index"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!await Index())
                {
                    throw new ApplicationException("'Help Index' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> Index()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task HelpOnlineHelp()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Online Help" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["HelpOnlineHelp"],
                    true,
                    33
                );

                //System.Windows.Forms.Help
                if (!await OnlineHelp())
                {
                    throw new ApplicationException("'Online Help' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> OnlineHelp()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task HelpLicenceInformation()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Licence Information" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["LicenceInformation"],
                    true,
                    33
                );

                if (!await LicenceInformation())
                {
                    throw new ApplicationException("'Licence Information' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected static async Task<bool> LicenceInformation()
        {
            bool returnValue = default;

            try
            {
                await DoSomethingElse();
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        public async Task HelpCheckForUpdates()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Check For Updates" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["CheckForUpdates"],
                    true,
                    33
                );

                if (!await CheckForUpdates())
                {
                    //throw new ApplicationException("'Check For Updates' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        protected async Task<bool> CheckForUpdates()
        {
            bool returnValue = default;
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> questionMessageDialogInfo = null;
            string errorMessage = null;

            try
            {
                questionMessageDialogInfo =
                    new MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes>
                    (
                        parent: View,//saved Window reference
                        modal: true,
                        title: "Check for Updates",
                        dialogFlags: null,
                        messageType: ImageLoader.Get("Info.png"),//Question is not available
                        buttonsType: DialogBoxForm.ButtonTypes.YesNo,
                        message: "Check for Updates?",
                        response: DialogResult.None
                    );

                questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);

                if (!questionMessageDialogInfo.BoolResult)
                {
					errorMessage = questionMessageDialogInfo.ErrorMessage;
					throw new ApplicationException(string.Format("CheckForUpdates: {0}", errorMessage));
                }

                if (questionMessageDialogInfo.Response != DialogResult.None)
                {
                    if (questionMessageDialogInfo.Response == DialogResult.Yes)
                    {
                        //TODO:do something with answer
						UpdateStatusBarMessages(StatusMessage + "Check: Yes" + ACTION_IN_PROGRESS, null);
                        returnValue = true;
                    }
                    else
                    {
                        //treat as cancel
						UpdateStatusBarMessages(StatusMessage + "Check: No" + ACTION_IN_PROGRESS, null);
                        returnValue = true;
                    }
                }
                else
                {
                    //treat as cancel
                    UpdateStatusBarMessages(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS, null);
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
				questionMessageDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Form, string, string> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Font...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                string fontDescription = null;
                fontDialogInfo = new FontDialogInfo<Form, string, string>
                (
                    View,
                    true,
                    "Select Font",
                    null,
                    fontDescription
                );

                if (Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    if (fontDialogInfo.Response == "Ok")
                    {
                        StatusMessage += fontDialogInfo.FontDescription;
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Form, string, string> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Color...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                string color = null;
                colorDialogInfo = new ColorDialogInfo<Form, string, string>
                (
                    View,
                    true,
                    "Select Color",
                    null,
                    color
                );

                if (Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    if (colorDialogInfo.Response == "Ok")
                    {
                        StatusMessage +=
                            string.Format
                            (
                                "Red/Green/Blue:{0}",
                                colorDialogInfo.Color
                            );
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }
        #endregion Methods

    }
}
