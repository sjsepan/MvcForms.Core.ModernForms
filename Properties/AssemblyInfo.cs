﻿using System.Reflection;
// using System.Runtime.InteropServices;
using Ssepan.Application.Core;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("MvcForms.Core.ModernForms")]
[assembly: AssemblyDescription("Desktop GUI app prototype, on Linux, in C# / DotNet[5+] / ModernForms and SkiaSharp, using VSCode")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Free Software Foundation, Inc.")]
[assembly: AssemblyProduct("MvcForms.Core.ModernForms")]
[assembly: AssemblyCopyright("Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("0.4")]
namespace MvcForms.Core.ModernForms
{
    #region " Helper class to get information for the About form. "
    /// <summary>
    /// This class uses the System.Reflection.Assembly class to
    /// access assembly meta-data
    /// This class is ! a normal feature of AssemblyInfo.cs
    /// </summary>
    public class AssemblyInfo : AssemblyInfoBase<Modern.Forms.Form>
    {
        // Used by Helper Functions to access information from Assembly Attributes
        public AssemblyInfo()
        {
			myType = typeof(MvcView);
			Website = "https://gitlab.com/sjsepan/MvcForms.Core.AvaloniaUI";
        }
    }
    #endregion
}